class Kendaraan {
    constructor(jenisKendaraan, negaraPembuat) {
        this.jenisKendaraan = jenisKendaraan;
        this.negaraPembuat = negaraPembuat;
    }
    info() {
        console.log(`Jenis Kendaraan roda ${this.jenisKendaraan} dari negara ${this.negaraPembuat}`);
    }
}
class Mobil extends Kendaraan {
    // add constructor
    constructor(jenisKendaraan, negaraPembuat, merek, harga, persenPajak) {
        super(jenisKendaraan, negaraPembuat);
        this.merek = merek;
        this.harga = harga;
        this.persenPajak = persenPajak;
    }
    totalHarga() {
        return this.harga + (this.harga * (this.persenPajak / 100));
    }
    info() {
        super.info();
        console.log(`Kendaraan ini nama mereknya ${this.merek} dengan total harga Rp. ${this.totalHarga()}`);
    }
}

const kendaraan = new Kendaraan(2, "Jepang");
kendaraan.info();

const lamborghini = new Mobil(4, "Indonesia", "Mobil-45", 1000, 50);
lamborghini.info();